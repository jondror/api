<?php
class Controllers_Users extends RestController {
	private $_con;
	private function DBconnect(){
		$this->_con=mysqli_connect("localhost","jonathan_api","15053","jonathan_api");
	}
	public function get() {
		$this->DBconnect();
		//var_dump($this->request['params']['id']);
		//die();
		if (isset($this->request['params']['id'])){ //an id was recieved - return user
			if(!empty($this->request['params']['id'])) {
				$sql="SELECT * FROM users WHERE id=?";	
					//SQL statments method
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("s",$this->request['params']['id']);
				$stmt->execute();
				$stmt->store_result();				
				$stmt->bind_result($id, $name,$email);
				$stmt->fetch();
				$result = ['id'=>$id,'name'=>$name,'email'=>$email];
				$this->response = array('result' =>$result );
				$this->responseStatus = 200;
			} else { //No id was recieved - return all users
				$sql="SELECT * FROM users";
				$usersSqlResult = mysqli_query($this->_con,$sql);
				$user = [];
				while ($user = mysqli_fetch_array($usersSqlResult)){
					$users[] = ['name' => $user['name'],'email' => $user['email']];
				}
				$this->response = array('result' => $users);
				$this->responseStatus = 200;
			}	
		}else{
			$this->response = array('result' =>'Wrong parameters for users' );
			$this->responseStatus = 200;			
		}
	}
	public function post() {
		$this->response = array('result' => 'no post implemented for users');
		$this->responseStatus = 201;
	}
	public function put() {
		$this->response = array('result' => 'no put implemented for users');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for users');
		$this->responseStatus = 200;
	}
}
